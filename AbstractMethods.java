//A method does not have body is called Abstract method,and the class that is declared as abstract using abstract keyword is 
// called abstract class.If a class contains abstract method,it must be declared as abstract
 abstract public class AbstractMethods {
 abstract void m1();
 public static void main(String[] args) {
	 //AbstractMethods A=new AbstractMethods();
}
}
