import java.util.HashSet;
public class Hashset {
	public static void main(String[] args) {
		HashSet<Book> Set=new HashSet<Book>();
		  Book b1=new Book(101,"Java","James Gosling","Innominds",100);  
		    Book b2=new Book(102,"Data Communications & Networking","Forouzan","Mc Graw Hill",4);  
		    Book b3=new Book(103,"Operating System","Galvin","Wiley",6);  
		    //Adding Books to HashSet  
		    Set.add(b1);  
		    Set.add(b2);  
		    Set.add(b3);  
		    //Traversing HashSet  
		    for(Book b : Set){  
		    System.out.println(b.id+" "+b.name+" "+b.author+" "+b.publisher+" "+b.quantity);  
		    }  
		}  
		
	}


